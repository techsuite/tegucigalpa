import app.model.PingEvent;
import app.model.Mensaje;

public class testGenerales {
    
    public static void main(String[] args) {

        // =============================================================================
        // Test Mensaje.java
        Mensaje err1 = new Mensaje(1);
        Mensaje err2 = new Mensaje(2);
        err1.activaMsj();
        err2.activaMsj();
        
        // =============================================================================
        System.out.println("\n ------------------ Test Mensaje.java ------------------ ");

        // =============================================================================
        // Test PingEvent.java
        // =============================================================================
        System.out.println("\n ------------------ Test PingEvent.java ------------------ ");
        PingeaThread aaa = new PingeaThread();
        EsperaThread bb = new EsperaThread();
        aaa.start();
        bb.start();

        // Forzamos a que se esperen para terminar el main
        try {
            bb.join();
            aaa.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            aaa.interrupt();
            bb.interrupt();
        }
        System.out.println("FIN");
    }
}

/**
 * Clase auxiliar para comprobar el funcionamiento del PingEvent
 */
class EsperaThread extends Thread {
    @Override
    public void run() {
        try {
            java.lang.Thread.sleep(60000, 0); // Sleep 1 min
        } catch (InterruptedException ie) {
            ie.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
}

/**
 * Clase auxiliar para comprobar el funcionamiento del PingEvent
 */
class PingeaThread extends Thread {
    @Override
    public void run() {
        // 4'5 segundos entre pings ok
        boolean activado = PingEvent.activarPingEvent(4500);
        System.out.println("Activado -thread_lanza_pings_cada_4'5s : " + activado);

       
        
    }
}
