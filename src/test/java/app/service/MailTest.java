package app.service;

import static org.mockito.Mockito.*;

import app.factory.SimpleFactory;
import javax.mail.internet.MimeMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTest {

  @InjectMocks
  private Mail mail;

  @Mock
  private JavaMailSender mailSender;

  @Mock
  private SimpleFactory simpleFactory;

  @Mock
  private MimeMessage mimeMessage;

  @Mock
  private MimeMessageHelper mimeMessageHelper;

  @Test
  public void sendMailWorks() throws Exception {
    when(mailSender.createMimeMessage()).thenReturn(mimeMessage);
    when(simpleFactory.mimeMessageHelper(any())).thenReturn(mimeMessageHelper);

    mail.sendMail(
      "developer.techsuite@outlook.com",
      "Probando, probando",
      "<div><h1>Techsuite</h1><p>Esto es una prueba ..</p></div>"
    );

    verify(mimeMessageHelper).setTo("developer.techsuite@outlook.com");
    verify(mimeMessageHelper).setSubject("Probando, probando");
    verify(mimeMessageHelper)
      .setText("<div><h1>Techsuite</h1><p>Esto es una prueba ..</p></div>", true);
    verify(mailSender).send(mimeMessage);
  }
}
