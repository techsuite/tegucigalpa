package app.service;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestConexionManagua {
    @Autowired
    private ConexionManagua conexionManagua;
 
    @MockBean
    private OkHttpClient client;

    @Mock
    private LogsService logsService;

    @Mock
    private Call call;

    @Mock
    private Response response;

    @Mock
    private ResponseBody body;

    @Test
    public void conexionManagua_test2() throws Exception {
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(true);

        conexionManagua.connManagua();
        verify(logsService, times(0)).pingError(anyInt());
    }

    @Test(expected = IOException.class)
    public void conexionManagua_test3() throws Exception {
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(false);

        conexionManagua.connManagua();
    }

}
