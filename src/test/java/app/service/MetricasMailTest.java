package app.service;

import static org.assertj.core.api.Assertions.assertThat;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class MetricasMailTest {

//  @Autowired
//  private MetricasMail metricasMail;
//
//  @Before
//  public void setup() {
//    metricasMail.restart();
//  }
  /*
  @Test
  public void updateWorks() {
    Empleado dummyEmpleado = new Empleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "passwordDoctor1",
      "nombreUsuarioDoctor1"
    );
    Paciente dummyPaciente = new Paciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "birthdayPaciente1",
      "emailPaciente1",
      "genderPaciente1"
    );
    Pcr dummyPcr = new Pcr(
      "dniPacientePcr1",
      "dniDoctorPcr1",
      "fluorescenciaPcr1",
      "fechaPcr1"
    );
    AnalisisSangre dummyBloodTest = new AnalisisSangre(
      "dniPacienteSangre1",
      "dniDoctorSangre1",
      "electrolitosSangre1",
      "glucosaSangre1",
      "colesterolSangre1",
      "trigliceridosSangre1",
      "bilirrubinaSangre1",
      "fechaSangre1"
    );

    SidneyData dummy = new SidneyData(
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyBloodTest),
      List.of(dummyPcr)
    );

    metricasMail.update(dummy);

    assertThat(metricasMail.getNumBloodTest()).isEqualTo(1);
    assertThat(metricasMail.getNumPcr()).isEqualTo(1);

    metricasMail.restart();
  }

  @Test
  public void updateIfCasesWork() {
    Empleado dummyEmpleado = new Empleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "passwordDoctor1",
      "nombreUsuarioDoctor1"
    );
    Paciente dummyPaciente = new Paciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "birthdayPaciente1",
      "emailPaciente1",
      "genderPaciente1"
    );
    Pcr dummyPcr = new Pcr(
      "dniPacientePcr1",
      "dniDoctorPcr1",
      "fluorescenciaPcr1",
      "fechaPcr1"
    );
    AnalisisSangre dummyBloodTest = new AnalisisSangre(
      "dniPacienteSangre1",
      "dniDoctorSangre1",
      "electrolitosSangre1",
      "glucosaSangre1",
      "colesterolSangre1",
      "trigliceridosSangre1",
      "bilirrubinaSangre1",
      "fechaSangre1"
    );

    SidneyData dummy = new SidneyData(
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyBloodTest),
      List.of(dummyPcr)
    );

    metricasMail.update(dummy);

    assertThat(metricasMail.getNumBloodTest()).isEqualTo(1);
    assertThat(metricasMail.getNumPcr()).isEqualTo(1);

    metricasMail.restart();
  }

  @Test
  public void gettersWork() {
    Empleado dummyEmpleado = new Empleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "passwordDoctor1",
      "nombreUsuarioDoctor1"
    );
    Paciente dummyPaciente = new Paciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "birthdayPaciente1",
      "emailPaciente1",
      "genderPaciente1"
    );
    Pcr dummyPcr = new Pcr(
      "dniPacientePcr1",
      "dniDoctorPcr1",
      "fluorescenciaPcr1",
      "fechaPcr1"
    );
    AnalisisSangre dummyBloodTest = new AnalisisSangre(
      "dniPacienteSangre1",
      "dniDoctorSangre1",
      "electrolitosSangre1",
      "glucosaSangre1",
      "colesterolSangre1",
      "trigliceridosSangre1",
      "bilirrubinaSangre1",
      "fechaSangre1"
    );

    SidneyData dummy = new SidneyData(
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyBloodTest),
      List.of(dummyPcr)
    );

    metricasMail.update(dummy);

    assertThat(metricasMail.getNumBloodTest()).isEqualTo(1);
    assertThat(metricasMail.getNumPcr()).isEqualTo(1);

    metricasMail.restart();
  }

  @Test
  public void restartWorks() {
    Empleado dummyEmpleado = new Empleado(
      "dniDoctor1",
      "nombreDoctor1",
      "apellidosDoctor1",
      "passwordDoctor1",
      "nombreUsuarioDoctor1"
    );
    Paciente dummyPaciente = new Paciente(
      "dniPaciente1",
      "nombrePaciente1",
      "apellidosPaciente1",
      "birthdayPaciente1",
      "emailPaciente1",
      "genderPaciente1"
    );
    Pcr dummyPcr = new Pcr(
      "dniPacientePcr1",
      "dniDoctorPcr1",
      "fluorescenciaPcr1",
      "fechaPcr1"
    );
    AnalisisSangre dummyBloodTest = new AnalisisSangre(
      "dniPacienteSangre1",
      "dniDoctorSangre1",
      "electrolitosSangre1",
      "glucosaSangre1",
      "colesterolSangre1",
      "trigliceridosSangre1",
      "bilirrubinaSangre1",
      "fechaSangre1"
    );

    SidneyData dummy = new SidneyData(
      List.of(dummyEmpleado),
      List.of(dummyPaciente),
      List.of(dummyBloodTest),
      List.of(dummyPcr)
    );

    metricasMail.update(dummy);

    metricasMail.restart();

    assertThat(metricasMail.getNumBloodTest()).isEqualTo(0);
    assertThat(metricasMail.getNumPcr()).isEqualTo(0);
  }*/
}
