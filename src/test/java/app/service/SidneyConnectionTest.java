package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SidneyConnectionTest {

    //Creamos una conexion
    @Autowired
    private SidneyConnection sidneyConnection;
  
    //Creamos una llamada
    @Mock
    private Call call;
  
    //Creamos una respuesta
    @Mock
    private Response response;
  
    //Creamos el cuerpo de la respuesta
    @Mock
    private ResponseBody body;
  
    //Testeamos
    @Test
    public void connect_test() throws Exception {
      when(call.execute()).thenReturn(response); //Tras cogersela, le respondemos
      when(response.code()).thenReturn(200); //Le damos el codigo 200
      when(response.body()).thenReturn(body); //devolvemos la respuesta
      when(body.string()).thenReturn("correcto"); //le indicamos que funciona
  
      sidneyConnection.connect(200);
    }
}