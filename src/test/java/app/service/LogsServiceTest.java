package app.service;

import nl.altindag.log.LogCaptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LogsServiceTest {

    @MockBean
    private LogsService logsService;


    @Test
    public void firstExtractionTest(){
        String expectedStartMessage = "======= INICIO EXTRACCION PAGINA 1 =======";
        String expectedEndMessage = "======= EXTRACCION PAGINA 1 COMPLETADA =======";
        LogCaptor logCaptor = LogCaptor.forClass(LogsService.class);
        LogsService logsService = new LogsService();
        logsService.firstExtractionStart();
        assertThat(logCaptor.getInfoLogs().equals(expectedStartMessage));
        logsService.firstExtractionEnd();
        assertThat(logCaptor.getInfoLogs().equals(expectedEndMessage));
        }

    @Test
    public void nExtractionTest(){
        String expectedStartMessage = "======= INICIO EXTRACCION PAGINA 2 DE 3 =======";
        String expectedEndMessage = "======= EXTRACCION PAGINA 2 DE 3 COMPLETADA =======";
        LogCaptor logCaptor = LogCaptor.forClass(LogsService.class);
        LogsService logsService = new LogsService();
        logsService.nExtractionStart(2,3);
        assertThat(logCaptor.getInfoLogs().equals(expectedStartMessage));
        logsService.nExtractionEnd(2,3);
        assertThat(logCaptor.getInfoLogs().equals(expectedEndMessage));
    }

    @Test
    public void nTransformTest(){
        String expectedStartMessage = "======= INICIO TRANSFORMACION PAGINA 2 DE 3 =======";
        String expectedEndMessage = "======= TRANSFORMACION PAGINA 2 DE 3 COMPLETADA =======";
        LogCaptor logCaptor = LogCaptor.forClass(LogsService.class);
        LogsService logsService = new LogsService();
        logsService.nTransformStart(2,3);
        assertThat(logCaptor.getInfoLogs().equals(expectedStartMessage));
        logsService.nTransformEnd(2,3);
        assertThat(logCaptor.getInfoLogs().equals(expectedEndMessage));
    }

    @Test
    public void nLoadTest(){
        String expectedStartMessage = "======= INICIO ENVIO SIDNEY PAGINA 2 DE 3 =======";
        String expectedEndMessage = "======= ENVIO PAGINA 2 DE 3 COMPLETADA =======";
        LogCaptor logCaptor = LogCaptor.forClass(LogsService.class);
        LogsService logsService = new LogsService();
        logsService.nLoadStart(2,3);
        assertThat(logCaptor.getInfoLogs().equals(expectedStartMessage));
        logsService.nLoadEnd(2,3);
        assertThat(logCaptor.getInfoLogs().equals(expectedEndMessage));
    }

    @Test
    public void logsErrorsTest(){
        String expectedExtractionErrorMessage = "======= ERROR EN LA EXTRACCION. CODIGO ERROR404 =======";
        String expectedLoadErrorMessage = "======= ERROR EN EL ENVIO. CODIGO ERROR404 =======";
        String expectedPingErrorMessage = "Connection failed, error code: 404";
        LogCaptor logCaptor = LogCaptor.forClass(LogsService.class);
        LogsService logsService = new LogsService();
        logsService.pingError(404);
        assertThat(logCaptor.getInfoLogs().equals(expectedPingErrorMessage));
        logsService.extractionError(404);
        assertThat(logCaptor.getInfoLogs().equals(expectedExtractionErrorMessage));
        logsService.loadError(404);
        assertThat(logCaptor.getInfoLogs().equals(expectedLoadErrorMessage));
    }

    @Test
    public void basicTest() {
        String expectedBasicMessage = "Test operativo";
        LogCaptor logCaptor = LogCaptor.forClass(LogsService.class);
        LogsService logsService = new LogsService();
        logsService.basic("Test operativo");
        assertThat(logCaptor.getInfoLogs().equals(expectedBasicMessage));
    }

}
