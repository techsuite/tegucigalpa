package app.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import app.model.AnalisisSangre;
import app.model.Empleado;
import app.model.Paciente;
import app.model.Pcr;
import app.model.SidneyData;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoadServiceTest {
    
    @Autowired
    LoadService loadService;

    @Mock
    HttpService httpService;

    @Test
    public void mandarPaginaASidneyTest() throws Exception {

        List<Empleado> empleados = new ArrayList<>();
        List<Paciente> pacientes = new ArrayList<>();
        List<AnalisisSangre> sangre = new ArrayList<>();
        List<Pcr> cosa = new ArrayList<>();

        SidneyData sidneyData = new SidneyData();
        sidneyData.setBloodTest(sangre);
        sidneyData.setEmployees(empleados);
        sidneyData.setPatients(pacientes);
        sidneyData.setPcr(cosa);

        loadService.mandarPaginaASidney(sidneyData, true);
    }
}
