package app.service;

import static org.mockito.Mockito.*;

import app.model.ManaguaInfo;
import app.model.MetaData;
import app.model.SidneyData;
import app.model.response.ManaguaResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ETLServiceTest {

  @MockBean
  private ExtractionService extractionService;

  @Autowired
  private ETLService service;

  @MockBean
  private LoadService loadService;

  @MockBean
  private TranslateService translateService;

  @Test
  public void etlProcess_whenManaguaIsDown_shouldStop() throws Exception {
    when(extractionService.extraerPagina(anyInt(), anyBoolean()))
      .thenReturn(null);
    service.etlProcess();
    verify(extractionService, times(1)).extraerPagina(eq(1), anyBoolean());
  }

  @Test
  public void etlProcess_whenManaguaIsUP_shouldExtractPages() throws Exception {
    ManaguaResponse managuaResponse = new ManaguaResponse();
    managuaResponse.setData(mock(ManaguaInfo.class));
    managuaResponse.setInfoPage(new MetaData(10, 1, 7));

    when(extractionService.extraerPagina(anyInt(), anyBoolean()))
      .thenReturn(managuaResponse);

    service.etlProcess();

    verify(extractionService, atLeastOnce())
      .extraerPagina(anyInt(), anyBoolean());
    verify(translateService, atLeastOnce()).transform(any(ManaguaInfo.class));
    verify(loadService, atLeastOnce()).mandarPaginaASidney(any(), anyBoolean());
  }
}
