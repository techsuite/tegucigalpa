package app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.in;

import app.model.Empleado;
import app.model.ManaguaInfo;
import app.model.SidneyData;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TraduccionManaguaSidneyTest {

  @Autowired
  private TranslateService traductor;

  @Test
  public void transform_whenCalled_shouldTransformCorrectly() {
    ManaguaInfo info = new ManaguaInfo();
    info.setEmployees(
      "dniDoctor:11111111L;nombreDoctor:David;apellidosDoctor:Hernández;password:ciencia1;nombreUsuario:davi;"
    );
    info.setPatients(
      "dniPaciente:22222222L;nombrePaciente:Peito;apellidosPaciente:Pérez;birthday:1994-07-29;email:pepito@gmail.com;gender:Hombre;"
    );
    info.setBloodTest(
      "dniPaciente:;dniDoctor:;electrolitos:;glucosa:;colesterlol:;trigliceridos:;bilirrubina:;fecha:;"
    );
    info.setPcr(
      "dniPaciente:22222222L,22222222L;dniDoctor:11111111L,11111111Z;fluorescencia:80,4;fecha:2021-04-29,2021-04-29;"
    );

    SidneyData output = traductor.transform(info);
    assertThat(output.getEmployees()).hasSize(1);
    assertThat(output.getPatients()).hasSize(1);
    assertThat(output.getBloodTest()).hasSize(0);
    assertThat(output.getPcr()).hasSize(2);
  }
}
