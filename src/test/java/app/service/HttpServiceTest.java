package app.service;


import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import app.service.HttpService.Server;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class HttpServiceTest {
    
    @Autowired
    private HttpService servicio;

    @Mock
    private ResponseBody body;

    @Test
    public void HttpService_Test1 () throws Exception {

        Response respuesta;

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("pageSize", "20");
        queryParams.put("pageNumber", "1");
        queryParams.put("totalPages", "3");

        respuesta = servicio.get(Server.Managua, "/ruta-no-valida", queryParams);

    }

    @Test
    public void HttpService_Test3 () throws Exception {
        
        Response respuesta;

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("pageSize", "20");
        queryParams.put("pageNumber", "1");
        queryParams.put("totalPages", "3");

        respuesta = servicio.post(Server.Sidney, "tegucigalpaData", queryParams, "Hola");

    }

    @Test
    public void HttpServiceMissingBranch() throws Exception {
        
        Response respuesta;

        Map<String, String> queryParams = new HashMap<>();

        respuesta = servicio.get(Server.Managua, "/ruta-no-valida", queryParams);

    }


    
}
