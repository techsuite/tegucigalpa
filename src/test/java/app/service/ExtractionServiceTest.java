package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ExtractionServiceTest {


    @Autowired
    private ExtractionService extraction;

    @MockBean
    private HttpService httpService;


    @Mock
    private Call call;

    @Mock
    private Response response;

    @Mock
    private ResponseBody body;
    
    @Test
    public void extraccionMannaguaCorrecta() throws Exception {

        extraction.extraerPagina(1,true);
    }

    @Test
    public void extractionTegucigalpa_whenCalled_shouldntFinish() throws Exception {

        when(httpService.get(any(),any(),any())).thenThrow(NullPointerException.class);
        extraction.extraerPagina(1,true);

    }

}


