package app.controller;

import app.model.Busqueda;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ViewController {

  @GetMapping("/home")
  public String home(Model model) throws Exception {
    model.addAttribute(
      "busquedas",
      List.of(
        new Busqueda(1, "test 1", 100L),
        new Busqueda(2, "prueba 2", 300L)
      )
    );
    model.addAttribute("mensaje", "hola desde spring boot");
    return "home";
  }
}
