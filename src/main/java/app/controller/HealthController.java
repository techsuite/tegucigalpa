package app.controller;

import app.model.response.HealthResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

  @GetMapping("/status")
  public ResponseEntity<HealthResponse> alive() {
    HealthResponse healthResponse = new HealthResponse("alive v1");

    return ResponseEntity.ok().body(healthResponse);
  }

//  @GetMapping("/test")
//  public String test() {
//    return (
//      "{\n" +
//      "  \"data\": {\n" +
//      "    \"employees\": \"dniDoctor:50780097N,51325567S;nombreDoctor:Rafael,Sergio;apellidosDoctor:Núñez,Castro; password:1234,5678;nombreUsuario:rafaloko,sergio\",\n" +
//      "\n" +
//      "    \"patients\": \"dniPaciente:50987877D,49339802L;nombrePaciente:Ramón,Carmen;apellidosPaciente:Ortega,Gasset;birthday:23-03-2123,05-09-1492;email:ramonchu@gmail.com,carmencita@hotmail.es;gender:hombre,mujer\",\n" +
//      "\n" +
//      "    \"bloodTest\": \"dniPaciente:50987877D,49339802L;dniDoctor:50780097N,51325567S;electrolitos:149,123;glucosa:101,113;colesterol:162,112;trigliceridos:150,171;bilirrubina:1.1,0.2\",\n" +
//      "\n" +
//      "    \"pcr\": \"dniPaciente:50987877D,49339802L;dniDoctor:50780097N,51325567S;fluorescencia:0,2.8\"\n" +
//      "  },\n" +
//      "  \"infoPagina\": {\n" +
//      "    \"pageSize\": 10,\n" +
//      "    \"pageNumber\": 2,\n" +
//      "    \"totalPages\": 7\n" +
//      "  }\n" +
//      "}"
//    );
//  }
}
