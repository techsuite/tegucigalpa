package app.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Service
public class SidneyConnection {

@Autowired
private OkHttpClient client;

//@RequestMapping(method = RequestMethod.POST)
public void connect(int id) throws IOException {

  //Creamos el json con los datos necesarios
  String json = "{\"senderID\":\"tegucigalpa\",\"number\":"+id+"}";

  //Creamos la request
  RequestBody body = RequestBody.create(json, MediaType.parse("application/json"));
  Request request = new Request.Builder()
    .url("https://sidney.juliocastrodev.duckdns.org/getNumb")
    .post(body)
    .build();
 
    //Hacemos la llamada
    Call call = client.newCall(request);
    Response response = call.execute();

    //Imprimimos la respuesta
    System.out.println(response.toString());
}

}












