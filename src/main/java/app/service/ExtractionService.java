package app.service;

import app.model.response.ManaguaResponse;
import com.google.gson.Gson;
import java.util.Map;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExtractionService {

  @Autowired
  private HttpService httpService;

  private static final String PAGE_SIZE = "20";

  // null si ha habido un error en la extracción
  public ManaguaResponse extraerPagina(Integer pageNumber, boolean retry) {
    ManaguaResponse res = null;
    try {
      Response pageResponse = httpService.get(
        HttpService.Server.Managua,
        "/analisis",
        Map.of("pageNumber", pageNumber + "", "pageSize", PAGE_SIZE)
      );

      res =
        new Gson()
        .fromJson(pageResponse.body().string(), ManaguaResponse.class);
    } catch (Exception e) {
      // Volvemos a intentar si el flag está activado
      if (retry) return extraerPagina(pageNumber, false);
    }

    return res;
  }
}
