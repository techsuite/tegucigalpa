package app.service;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HttpService {

  @Autowired
  private OkHttpClient okHttpClient;

  @AllArgsConstructor
  @Getter
  public enum Server {
    Sidney("https", "sidney.juliocastrodev.duckdns.org"), 
    Managua("https", "managua-be.juliocastrodev.duckdns.org"); 

    private String scheme;
    private String host;
  }

  public Response get(
    Server server,
    String endpoint,
    Map<String, String> queryParams
  ) throws Exception {
    Request request = new Request.Builder()
      .url(buildUrl(server, endpoint, queryParams))
      .build();

    return okHttpClient.newCall(request).execute();
  }
  
  private static final MediaType JSON = MediaType.get(
    "application/json; charset=utf-8"
  );

  public Response post(
    Server server,
    String endpoint,
    Map<String, String> queryParams,
    String body
  ) throws Exception {
    Request request = new Request.Builder()
      .url(buildUrl(server, endpoint, queryParams))
      .post(RequestBody.create(body, JSON))
      .build();

    return okHttpClient.newCall(request).execute();
  }


  private HttpUrl buildUrl(
    Server server,
    String endpoint,
    Map<String, String> queryParams
  ) {
    HttpUrl.Builder builder = new HttpUrl.Builder()
      .scheme(server.getScheme())
      .host(server.getHost())
      .addPathSegments(endpoint);

    if (queryParams != null && queryParams.size() > 0) {
      queryParams.forEach(builder::addQueryParameter);
    }

    return builder.build();
  }
}
