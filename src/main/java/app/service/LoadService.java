package app.service;

import app.model.SidneyData;
import app.service.HttpService.Server;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoadService {

  @Autowired
  private HttpService httpService;

  public void mandarPaginaASidney(SidneyData data, boolean retry) {
    try {
      String body = new GsonBuilder().setPrettyPrinting().create().toJson(data);
      System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(data));
      httpService.post(Server.Sidney, "/getDataTegucigalpa", null, body);
    } catch (Exception exception) {
      if (retry) mandarPaginaASidney(data, false);
    }
  }
}
