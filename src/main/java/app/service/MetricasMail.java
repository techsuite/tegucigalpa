package app.service;
import app.model.SidneyData;
import org.springframework.stereotype.Service;
import lombok.Getter;

@Getter
@Service
public class MetricasMail {
    private int numBloodTest;
    private int numPcr;
    private int numEmp;
    private int numPat;

    public void update(SidneyData datos) {
        numBloodTest = numBloodTest + datos.getBloodTest().size();
        numPcr = numPcr + datos.getPcr().size();
        numEmp = numEmp + datos.getEmployees().size();
        numPat = numPat + datos.getPatients().size();
    }

    public void restart() {
        numBloodTest = 0;
        numPcr = 0;
        numEmp = 0;
        numPat = 0;
    }
}
