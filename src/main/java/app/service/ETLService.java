package app.service;

import app.model.MetaData;
import app.model.response.ManaguaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ETLService {

  @Autowired
  private ExtractionService extractionService;

  @Autowired
  private TranslateService traduccionService;

  @Autowired
  private LoadService loadService;

 @Scheduled(cron = "0 0 */1 * * *")
  // @Scheduled(fixedRate = 180000)
  public void etlProcess() throws Exception {
    System.out.println("TRABAJANDO CON PÁGINA 1");
    MetaData metaData = extractTransformLoad(1);
    if (metaData == null) {
      // Ha habido un problema con la extracción de la primera, no seguimos
      return;
    }

    // Pillamos el resto
    for (int i = 2; i <= metaData.getTotalPages(); i++) {
      System.out.printf("TRABAJANDO CON PÁGINA %d\n",i);
      extractTransformLoad(i);
    }
  }

  // devuelve la metadata si todo ok y null en otro caso
  private MetaData extractTransformLoad(Integer pageNumber) throws Exception {
    ManaguaResponse pagina = extractionService.extraerPagina(pageNumber, true);
    if (pagina == null) return null;
    loadService.mandarPaginaASidney(
      traduccionService.transform(pagina.getData()),
      true
    );
    return pagina.getInfoPage();
  }
}
