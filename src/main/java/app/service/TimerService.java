package app.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service

public class TimerService {
    @Autowired
    private MetricasMail metricasMail;

    @Autowired
    private Mail mail;

 @Scheduled(cron = "0 0 */1 * * *")
    public void metricSending() {
        try {
            mail.sendMail(
                    "developer.techsuite@outlook.com",
                    "Metricas análisis ultimas 24h",
                    "<div><h1>Techsuite</h1>" +
                            "<p>En las últimas 24h se han realizado: "
                            + metricasMail.getNumBloodTest() + " análisis de sangre,\n "
                            + metricasMail.getNumPcr() + " pruebas PCR,\n"
                            + metricasMail.getNumPat() + " pacientes han pasado por consulta y han sido atendidos por\n"
                            + metricasMail.getNumEmp() + " empleados");
        } catch (Exception e) {
            e.printStackTrace();
        }
        metricasMail.restart();
    }


}