package app.service;

import app.model.AnalisisSangre;
import app.model.Empleado;
import app.model.ManaguaInfo;
import app.model.Paciente;
import app.model.Pcr;
import app.model.SidneyData;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TranslateService {

  @Autowired
  private MetricasMail metricas;

  public SidneyData transform(ManaguaInfo json) {
    SidneyData sidneyData = new SidneyData();
    sidneyData.setEmployees(retrieveEmpleados(json));
    sidneyData.setPatients(retrievePacientes(json));
    sidneyData.setPcr(retrievePcrs(json));
    sidneyData.setBloodTest(retrieveAnalisisSangre(json));
    metricas.update(sidneyData);

    return sidneyData;
  }

  private List<Empleado> retrieveEmpleados(ManaguaInfo info) {
    List<String> dniDoctores = extractList(info.getEmployees(), "dniDoctor");
    List<String> nombreDoctores = extractList(
      info.getEmployees(),
      "nombreDoctor"
    );
    List<String> apellidosDoctores = extractList(
      info.getEmployees(),
      "apellidosDoctor"
    );
    List<String> passwords = extractList(info.getEmployees(), "password");
    List<String> nombreUsuarios = extractList(
      info.getEmployees(),
      "nombreUsuario"
    );

    List<Empleado> res = new ArrayList<>();
    for (int i = 0; i < dniDoctores.size(); i++) {
      Empleado empleado = new Empleado();
      empleado.setDni(dniDoctores.get(i));
      empleado.setNombre(nombreDoctores.get(i));
      empleado.setApellidos(apellidosDoctores.get(i));
      empleado.setPassword(passwords.get(i));
      empleado.setUsername(nombreUsuarios.get(i));

      res.add(empleado);
    }

    return res;
  }

  private List<Paciente> retrievePacientes(ManaguaInfo info) {
    List<String> dniPacientes = extractList(info.getPatients(), "dniPaciente");
    List<String> nombrePacientes = extractList(
      info.getPatients(),
      "nombrePaciente"
    );
    List<String> apellidosPacientes = extractList(
      info.getPatients(),
      "apellidosPaciente"
    );
    List<String> birthdays = extractList(info.getPatients(), "birthday");
    List<String> emails = extractList(info.getPatients(), "email");
    List<String> genders = extractList(info.getPatients(), "gender");

    List<Paciente> res = new ArrayList<>();
    for (int i = 0; i < dniPacientes.size(); i++) {
      Paciente paciente = new Paciente();
      paciente.setDni(dniPacientes.get(i));
      paciente.setNombre(nombrePacientes.get(i));
      paciente.setApellidos(apellidosPacientes.get(i));
      paciente.setFechaNacimiento(birthdays.get(i));
      paciente.setCorreoElectronico(emails.get(i));
      paciente.setGenero(
        "Hombre".equals(genders.get(i))
          ? "masculino"
          : "Mujer".equals(genders.get(i)) ? "femenino" : "x"
      );

      res.add(paciente);
    }

    return res;
  }

  private List<AnalisisSangre> retrieveAnalisisSangre(ManaguaInfo info) {
    List<String> dniPacientes = extractList(info.getBloodTest(), "dniPaciente");
    List<String> dniDoctores = extractList(info.getBloodTest(), "dniDoctor");
    List<String> electrolitos = extractList(
      info.getBloodTest(),
      "electrolitos"
    );
    List<String> glucosas = extractList(info.getBloodTest(), "glucosa");
    List<String> colesteroles = extractList(info.getBloodTest(), "colesterlol");
    List<String> trigliceridos = extractList(
      info.getBloodTest(),
      "trigliceridos"
    );
    List<String> bilirrubinas = extractList(info.getBloodTest(), "bilirrubina");
    List<String> fechas = extractList(info.getBloodTest(), "fecha");

    List<AnalisisSangre> res = new ArrayList<>();
    for (int i = 0; i < dniPacientes.size(); i++) {
      AnalisisSangre analisisSangre = new AnalisisSangre();
      analisisSangre.setDoctorID(dniDoctores.get(i));
      analisisSangre.setPatientID(dniPacientes.get(i));
      analisisSangre.setElectrolitos(electrolitos.get(i));
      analisisSangre.setGlucosa(glucosas.get(i));
      analisisSangre.setColesterol(colesteroles.get(i));
      analisisSangre.setTrigliceridos(trigliceridos.get(i));
      analisisSangre.setBilirrubina(bilirrubinas.get(i));
      analisisSangre.setFecha(fechas.get(i));

      res.add(analisisSangre);
    }

    return res;
  }

  private List<Pcr> retrievePcrs(ManaguaInfo info) {
    List<String> dniPacientes = extractList(info.getPcr(), "dniPaciente");
    List<String> dniDoctores = extractList(info.getPcr(), "dniDoctor");
    List<String> fluorescencias = extractList(info.getPcr(), "fluorescencia");
    List<String> fechas = extractList(info.getPcr(), "fecha");

    List<Pcr> res = new ArrayList<>();
    for (int i = 0; i < dniPacientes.size(); i++) {
      Pcr pcr = new Pcr();
      pcr.setPatientID(dniPacientes.get(i));
      pcr.setDoctorID(dniDoctores.get(i));
      pcr.setFluorescencia(fluorescencias.get(i));
      pcr.setFecha(fechas.get(i));

      res.add(pcr);
    }
    return res;
  }

  private List<String> extractList(String formattedData, String field) {
    int indexOfField = formattedData.indexOf(field);
    String fieldDataWithKey = formattedData.substring( // field: dato1, dato2, dato3
      indexOfField,
      formattedData.indexOf(";", indexOfField)
    );
    String fieldData = fieldDataWithKey.substring( // dato1, dato2, dato3
      fieldDataWithKey.indexOf(":") + 1
    );

    return fieldData.length() > 0 ? List.of(fieldData.split(",")) : List.of();
  }
}
