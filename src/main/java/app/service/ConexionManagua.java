package app.service;

import java.io.IOException;
import java.net.http.HttpClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


//Clase conexión Managua de tipo GET
@Service
public class ConexionManagua {

  @Autowired
  OkHttpClient client;

  //Método que realiza el ping con Managua y no devuelve nada
  public void connManagua() throws java.io.IOException {
    //Obtenemos request
    Request request = new Request.Builder()
      .url("https://managua-be.juliocastrodev.duckdns.org/status")
      .build();
    //Obtenemos response
    Response response = client.newCall(request).execute();
    //Comprobación de si ha sido correcta la conexión
    //Caso negativo se lanza excepción
    if (!response.isSuccessful()) {
      throw new IOException("Error in the response: " + response);
    }
  }
}
