package app.model;

import java.util.List;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SidneyData {

  private List<Empleado> employees;
  private List<Paciente> patients;
  private List<AnalisisSangre> bloodTest;
  private List<Pcr> pcr;
  private String key = "qnxCE%MIA%m9";
}
