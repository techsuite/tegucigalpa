package app.model.response;

import app.model.ManaguaInfo;
import app.model.MetaData;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ManaguaResponse {

  //  private JSONObject consulta;
  private ManaguaInfo data;
  private MetaData infoPage;
}
