package app.model;

import javax.swing.Timer;
import lombok.Getter;
import java.awt.event.*;

public class PingEvent {

    private static Timer timer;
    private static int numPingMal = 0;
    public static @Getter int delay; // Tiempo de espera entre pings satisfactorios (en Milisegudos)
    public static boolean statusOk = false; // Indica si se están ejecutando los pings

    /**
     * Acciones a ejecutar si el ping no es satisfactorio
     */
    private static void sinRespuesta() {
        numPingMal++;
        // Volvemos a mandar un ping a los 20ms
        timer.setDelay(20);

        if (numPingMal > 4) {
            // TODO: Lanzamos error de que no hay conexion -------------------------------
            pararPingEvent();
        }
    }

    /**
     * Acciones a ejecutar si el ping es satisfactorio
     * 
     * @param tiempo - tiempo que ha tardado en devolver el ping
     */
    private static void conRespuesta(long tiempo) {
        numPingMal = 0; // Reiniciamos el contador de pings perdidos
        timer.setDelay(delay);
        // TODO: Hay que guardar el tiempo en algun lado -------------------------------
    }

    /**
     * Activa el lanzamiento de pings cada delay, utilizado para comprobar la
     * conexión correcta entre puertos del servidor
     * 
     * @param delayUser Tiempo de espera entre pings satisfactorios (en Milisegudos)
     * 
     * @return status del Evento (true si está activado, false si no lo está)
     */
    public static boolean activarPingEvent(int delayUser) {
        // Si el delay indicado es menos de 15ms, poner cada hora
        delay = (delayUser < 15) ? 3600000 : delayUser;

        ActionListener pingPerformer = new ActionListener() { // Evento que hace el ping
            // Accion que realmente se ejecuta al hacer ping cada x tiempo
            public void actionPerformed(ActionEvent evt) {

                long horaActual = System.currentTimeMillis();

                // TODO: HAY QUE CAMBIARLO CUANDO EL METODO PING ESTÉ HECHO ------------------
                boolean resPing = dummyPing();// ==================================================
                // Se me ocurre hacer algo del estilo:
                // InetAddress.getByName(nombre_servidor).isReachable(tiempo_max_ping);
                // HAY QUE CAMBIARLO CUANDO EL METODO PING ESTÉ HECHO ------------------------

                horaActual = System.currentTimeMillis() - horaActual;
                // Si el ping no es satisfactorio, lanzamos otro a los 15 ms
                if (!resPing)
                    sinRespuesta();
                else
                    conRespuesta(horaActual);
            }
        };

        // El timer lanza la accion especificada por el pingPerformer cada delay ms
        timer = new Timer(delay, pingPerformer);
        timer.start();
        statusOk = true;

        return statusOk;
    }

    /**
     * Para el lanzamiento de pings, utilizado para comprobar la conexión correcta
     * entre puertos del servidor
     * 
     * @return status del Evento (true si está activado, false si no lo está)
     */
    public static boolean pararPingEvent() {
        timer.stop();
        statusOk = false;
        return statusOk;
    }

    // =============================================================================
    // =============================================================================
    // Para probar el funcionamiento ----------------------\/-----------------------

    static int cuentaPingSatisfactorios = 0;

    public static boolean dummyPing() {
        int num = (int) (Math.random() * 10000) % 9;
        if (num%2 == 0) {
            cuentaPingSatisfactorios++;
            System.out.println(cuentaPingSatisfactorios + " Ping!!!   estoy dentro! ");
        } else System.out.println("ping MAL");
        return true;
    }
}
