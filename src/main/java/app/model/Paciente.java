package app.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Paciente {

  private String dni;
  private String nombre;
  private String apellidos;
  private String fechaNacimiento;
  private String correoElectronico;
  private String genero;
}

