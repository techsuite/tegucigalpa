package app.model;

public class Mensaje {
    public int code;

    public Mensaje(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void activaMsj() {
        switch (code) {
            case 1: // Error de conexión con Managua
                System.err.println("Error de conexión con el puerto 8004");
                // Cuando esté hecha la pagina de error: -------------------------------------
                // TODO: redirect(/lo_que_sea);
                break;

            case 2: // Error de conexión con Sidney
                System.err.println("Error de conexión con el puerto 8009");
                // Cuando esté hecha la pagina de error: -------------------------------------
                // TODO: redirect(/lo_que_sea);
                break;

            default: // Error genérico (Mala señal que salte :/)
                System.err.println("Error interno del servidor. Por favor contacte con los administradores.");
                break;
        }
    }
}
