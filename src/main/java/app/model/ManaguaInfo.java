package app.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ManaguaInfo {

  private String employees;
  private String patients;
  private String bloodTest;
  private String pcr;
}
