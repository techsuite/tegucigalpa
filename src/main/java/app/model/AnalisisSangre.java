package app.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class AnalisisSangre {
    
  private String patientID;
  private String doctorID;
  private String electrolitos;
  private String glucosa;
  private String colesterol;
  private String trigliceridos;
  private String bilirrubina;
  private String fecha;
}


