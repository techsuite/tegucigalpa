package app.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Pcr {
    
  private String patientID;
  private String doctorID;
  private String fluorescencia;
  private String fecha;

}
