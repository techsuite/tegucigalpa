package app.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Empleado {

  private String dni;
  private String nombre;
  private String apellidos;
  private String fechaNacimiento = "1990-01-01";
  private String correoElectronico = "fake@gmail.com";
  private String genero = "x";
  private String username = "undefined";
  private String password;
}
